package com.weather.servlets;

import com.weather.objects.WeatherNow;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by developer on 25.03.15.
 */
@WebServlet ("/GetWeather")
public class GetWeather extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetWeather() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub

        response.setContentType("text/html;charset=UTF-8");

        String cityName = request.getParameter("city");
        try{
            WeatherNow weatherNow = new WeatherNow(cityName);
            String tempC = weatherNow.getWeatherObject().getTempC();
            String weatherDesc = weatherNow.getWeatherObject().getWeatherDesc();
            String weatherIconUrl = weatherNow.getWeatherObject().getWeatherIconUrl();
            String windspeedKmph = weatherNow.getWeatherObject().getWindSpeedKmph();
            String humidity = weatherNow.getWeatherObject().getHumidity();
            String visibility = weatherNow.getWeatherObject().getVisibility();

            PrintWriter out = response.getWriter();
            //out.println(weatherNow.getXmlResponse());
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Tester</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("temp = " + tempC + "<br>");
            out.println("info = " + weatherDesc + "<br>");
            out.println("<img src=\"" + weatherIconUrl + "\"><br>");
            out.println("wind speed = " + windspeedKmph + "<br>");
            out.println("humidity = " + humidity + "<br>");
            out.println("visibility = " + visibility + "<br>");
            out.println("</body>");
            out.println("</html>");

        }catch(Exception ex){

        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
    }

}
