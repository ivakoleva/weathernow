package com.weather.objects;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class WeatherHttpRequester {

	private String response = "";

	public WeatherHttpRequester(String urlRequest) throws Exception {

		if (urlRequest == null || urlRequest.equalsIgnoreCase("")) {
			throw new Exception("WeatherHttpRequester exception - invalid url for request!");
		}

		URL url;
		HttpURLConnection connection = null;
		try {

			// Create connection
			url = new URL(urlRequest);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			connection.setRequestProperty("Content-Length", "" + Integer.toString(urlRequest.getBytes().length));
			connection.setRequestProperty("Content-Language", "en-US");
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);

			// Send request
			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			wr.writeBytes(urlRequest);
			wr.flush();
			wr.close();

			// Get Response
			InputStream is = connection.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			String line;
			StringBuffer response = new StringBuffer();
			while ((line = rd.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}
			rd.close();
			this.setResponse(response.toString());
		} catch (Exception e) {
			e.printStackTrace();
			this.setResponse("404 : " + e.getMessage());
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
	}

	public String getResponse() {
		return response;
	}

	private void setResponse(String response) {
		this.response = response;
	}
}
