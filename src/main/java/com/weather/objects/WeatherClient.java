package com.weather.objects;

public class WeatherClient {

	private static final String API_URL = "http://api.worldweatheronline.com/free/v1/weather.ashx?q=";
	private static final String API_KEY = "44ts4b8a95d84tq4j6zzyjnc";
	private static final String API_PARAMETERS = "&format=xml&extra=localObsTime2C+isDayTime2C+utcDateTime&num_of_days=5&includelocation=yes&key=";
	
	private String full_url = "";

	public WeatherClient(String cityName){
		this.setFull_url(cityName);		
	}

	public String getFull_url() {
		return full_url;
	}

	public void setFull_url(String cityName) {
		this.full_url = API_URL + cityName + API_PARAMETERS + API_KEY;
	}
}
